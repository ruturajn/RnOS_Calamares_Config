<div align="center">

<img src="./assets/RnOS_Polygon_Final.png" width="150" height="150">

# The Calamares Installer for RnOS

<img src="./assets/Calamares-Welcome.png">

*For detailed installation instructions please refer to this [video](/assets/Calamares_Demo.mp4).*

</div>

# SPDX-FileCopyrightText: no
# SPDX-License-Identifier: CC0-1.0
#
# Configuration file for Calamares

modules-search: [ local ]

# A module that is often used with instances is shellprocess, which will
# run shell commands specified in the configuration file. By configuring
# more than one instance of the module, multiple shell sessions can be run
# during install.
#
# YAML: list of maps of string:string key-value pairs.
instances:
- id:       finishinst
  module:   shellprocess
  config:   shellprocess.conf
- id:       before
  module:   shellprocess
  config:   shellprocess-before.conf

# Sequence section. This section describes the sequence of modules, both
# viewmodules and jobmodules, as they should appear and/or run.
# It then executes the job queue and clears it. If a viewmodule offers up a
# job for execution, but the module name (or instance key) isn't listed in the
# immediately following exec phase, this job will not be executed.
#
# YAML: list of lists of strings.
sequence:
- show:
  - welcome
  - locale
  - keyboard
  - partition
  - users
  - summary
- exec:
  - partition
  - mount
  - unpackfs
  - machineid
  - fstab
  - locale
  - keyboard
  - localecfg
  - luksbootkeyfile
  - luksopenswaphookcfg
  - initcpiocfg
  - initcpio
  - removeuser
  - users
  - displaymanager
  - networkcfg
  - hwclock
  - services-systemd
  - grubcfg
  - shellprocess@before
  - bootloader
  - shellprocess@finishinst
  - umount
- show:
#  - webview@owncloud
  - finished

# Only the name of the branding component (directory) should be specified
# here, Calamares then takes care of finding it and loading the contents.
#
# YAML: string.
branding: RnOS

# If this is set to true, Calamares will show an "Are you sure?" prompt right
# before each execution phase, i.e. at points of no return. If this is set to
# false, no prompt is shown. Default is false, but Calamares will complain if
# this is not explicitly set.
#
# YAML: boolean.
prompt-install: true

# YAML: boolean.
dont-chroot: false

# If this is set to true, Calamares refers to itself as a "setup program"
# rather than an "installer". Defaults to the value of dont-chroot, but
# Calamares will complain if this is not explicitly set.
oem-setup: false

# If this is set to true, the "Cancel" button will be disabled entirely.
# The button is also hidden from view.
# YAML: boolean.
disable-cancel: false

# If this is set to true, the "Cancel" button will be disabled once
# you start the 'Installation', meaning there won't be a way to cancel
# the Installation until it has finished or installation has failed.
#
# Default is false, but Calamares will complain if this is not explicitly set.
#
# YAML: boolean.
disable-cancel-during-exec: false

# If this is set to true, the "Next" and "Back" button will be hidden once
# you start the 'Installation'.
#
# Default is false, but Calamares will complain if this is not explicitly set.
#
# YAML: boolean.
hide-back-and-next-during-exec: false

# If this is set to true, then once the end of the sequence has
# been reached, the quit (done) button is clicked automatically
# and Calamares will close. Default is false: the user will see
# that the end of installation has been reached, and that things are ok.
#
#
quit-at-end: false

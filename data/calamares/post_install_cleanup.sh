#!/bin/bash

# Author : Ruturajn <nanotiruturaj@gmail.com>

# This script performs some post installation procedure.

# Removing stuff, that was only used by the ISO.
rm /etc/systemd/system/etc-pacman.d-gnupg.mount
rm /root/.automated_script.sh
rm /root/.zlogin
rm /etc/polkit-1/rules.d/49-nopasswd_global.rules
rm -r /etc/systemd/system/getty@tty1.service.d
rm /usr/local/bin/Installation_guide

# Remove unwanted packages
pacman-key --init
pacman-key --populate archlinux
pacman -Syu --noconfirm --disable-download-timeout

# Remove GPU Drivers

# Check if NVIDIA GPU exists on the system
if [[ -z $(lspci -k | grep -E "VGA|3D" | grep "NVIDIA") ]]; then
    pacman -Rns nvidia nvidia-settings nvidia-utils xf86-video-nouveau --noconfirm
else
    pacman -Rns xf86-video-nouveau --noconfirm
fi

# Check if AMD GPU exists on the system
if [[ -z $(lspci -k | grep -E "VGA|3D" | grep "Advanced Micro Devices") || $("${lspci_gpu_info}" | grep -A 3 "AMD/ATI") || $("${lspci_gpu_info}" | grep -A 3 "Radeon") ]]; then
	pacman -Rns xf86-video-amdgpu xf86-video-ati --noconfirm
fi

# Check if Intel GPU exists on the system
if [[ -z $(lspci -k | grep -E "VGA|3D" | grep "Intel") ]] ; then
    pacman -Rns xf86-video-intel
fi

# Remove Other packages that are only required in the live environment.
pacman -Rns calamares ckbcomp rnos-calamares-config archinstall arch-install-scripts --noconfirm

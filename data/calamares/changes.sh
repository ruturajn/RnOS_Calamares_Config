#!/bin/bash

# Author : Ruturajn <nanotiruturaj@gmail.com>

# This script performs some modifications, required just before
# the installation completes.

# Change GRUB background and Distributor name
sed -i -e 's|^#GRUB_BACKGROUND=.*|GRUB_BACKGROUND=/usr/share/Wallpapers/RnOS_Grub_Background.png|' -e 's|^GRUB_DISTRIBUTOR=.*|GRUB_DISTRIBUTOR="RnOS"|' /etc/default/grub

# Modifying GRUB Menu Entry.
sed -i 's|OS="\${GRUB_DISTRIBUTOR} Linux"|OS="\${GRUB_DISTRIBUTOR}"|' /etc/grub.d/10_linux

# Get `username` for the non-root user.
user_name=$(cat /etc/passwd | grep "/home" | cut -d : -f 1)

# Change the path to dunst icons
sed -i "s|    icon_path = .*|    icon_path = $user_name/.config/dunst/icons|" /home/"${user_name}"/.config/dunst/dunstrc

# Make changes based on whether installation is being done on a VM.
if [[ $(systemd-detect-virt) != "none" ]] ; then
	sed -i 's/size\: 10/size\: 14/' /home/"${user_name}"/.config/alacritty/alacritty.yml
else
	sed 's|^picom.*|picom \&|' /home/"${user_name}"/config/qtile/autostart.sh
fi

# Fix username capitalization issue - sddm
sed -i 's|font.capitalization: config.AllowBadUsernames == "false" ? Font.Capitalize : Font.MixedCase|// font.capitalization: config.AllowBadUsernames == "false" ? Font.Capitalize : Font.MixedCase|' /usr/share/sddm/themes/Sugar-Candy/Components/Input.qml
